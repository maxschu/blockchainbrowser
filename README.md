# README #

### What is this repository for? ###

This repository is the solution for the developer assessment for the application of Maximilian Schubert to Argent Labs

### Questions to Answer: ###

#### How long did you spend on the exercise? ####
About 8 hours

#### What would you improve if you had more time? ####
* Implement a proper database
* More clean reactive setup for api calls
* Simpler balance presenter, strip some functionality into other classes

#### What would you like to highlight in the code? ####
MVP Architecture (a bit overkill and much overhead for such a small project, but i really like the project setup and code readability with MVP)

#### If you had to store the private key associated to this ethereum account address, how would you make that storage secure ####
If possible don't store it on the phone. Store it in a secluded space from the app, maybe provide a cloud or something similar for that. (Data on the phone can always be accessed if you know how to crack it)
Otherwise encrypt it and store it in the Android Keystore.


### Who do I talk to? ###

Owner: Maximilian Schubert