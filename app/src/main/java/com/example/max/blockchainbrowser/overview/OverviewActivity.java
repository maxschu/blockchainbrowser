package com.example.max.blockchainbrowser.overview;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.AppFlowManager;
import com.example.max.blockchainbrowser.core.BaseActivity;
import com.example.max.blockchainbrowser.core.Injector;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OverviewActivity extends BaseActivity implements LoadingCallback {

    @Inject
    AppFlowManager appFlowManager;
    @BindView(R.id.balance_account)
    BalanceView balanceViewAccount;
    @BindView(R.id.balance_erc)
    BalanceView balanceViewERC;
    @BindView(R.id.loading_spinner)
    FrameLayout loadingSpinner;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.overview_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        Injector.getActivityComponent()
                .inject(this);
        balanceViewAccount.getPresenter().loadAccountBalance();
        balanceViewERC.getPresenter().loadData(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.view_more_button)
    void onMoreClick() {
        appFlowManager.openTokenList();
    }

    @OnClick(R.id.refresh_button)
    void onRefreshClick() {
        loadingSpinner.setVisibility(View.VISIBLE);
        balanceViewAccount.getPresenter().loadAccountBalance();
        balanceViewERC.getPresenter().loadData(this);
    }

    @Override
    public void onLoadingFinished() {
        loadingSpinner.setVisibility(View.GONE);
    }

}
