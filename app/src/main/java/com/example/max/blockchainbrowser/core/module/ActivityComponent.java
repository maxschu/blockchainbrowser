package com.example.max.blockchainbrowser.core.module;

import com.example.max.blockchainbrowser.core.BaseActivity;
import com.example.max.blockchainbrowser.overview.BalancePresenter;
import com.example.max.blockchainbrowser.overview.OverviewActivity;
import com.example.max.blockchainbrowser.tokens.TokenListPresenter;

import dagger.Subcomponent;

@ActivityScope
@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(BaseActivity baseActivity);

    void inject(OverviewActivity overviewActivity);

    void inject(BalancePresenter balancePresenter);

    void inject(TokenListPresenter tokenListPresenter);
}
