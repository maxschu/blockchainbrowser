package com.example.max.blockchainbrowser.core.module;

import android.content.Context;

import com.example.max.blockchainbrowser.core.BaseApplication;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component (modules = ApplicationModule.class)
public interface AppComponent {

   BaseApplication application();

   Context context();

   ActivityComponent plus(ActivityModule activityModule);
}
