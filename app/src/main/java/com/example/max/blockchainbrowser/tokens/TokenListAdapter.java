package com.example.max.blockchainbrowser.tokens;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.ViewHolderPresenterView;
import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;

import java.util.List;

class TokenListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

   private List<TokenListItemViewModel> tokenList;

   void addTokenList(List<TokenListItemViewModel> tokenList) {
      if (this.tokenList == null) {
         this.tokenList = tokenList;
      } else {
         this.tokenList.addAll(tokenList);
      }
      notifyDataSetChanged();
   }

   @Override
   public int getItemCount() {
      int itemCount = 0;
      if (tokenList != null) {
         itemCount = tokenList.size();
      }
      return itemCount;
   }

   @Override
   public int getItemViewType(int position) {
      return R.layout.token_list_item;
   }

   @SuppressWarnings ("unchecked")
   @Override
   public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      final TokenListItemViewModel product = tokenList.get(position);
      ((ViewHolderPresenterView.ViewHolder) holder).bindViewHolder(product);
   }

   @Override
   public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      TokenListItemView itemView = (TokenListItemView) LayoutInflater.from(parent.getContext())
            .inflate(R.layout.token_list_item, parent, false);

      itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT));

      return itemView.getViewHolder();
   }
}
