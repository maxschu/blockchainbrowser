package com.example.max.blockchainbrowser.data.models;

public class BalanceViewModel {
   private String balance;
   private String name;

   public BalanceViewModel(String name, String balance) {
      this.name = name;
      this.balance = balance;
   }

   public String getBalance() {
      return balance;
   }

   public String getName() {
      return name;
   }
}
