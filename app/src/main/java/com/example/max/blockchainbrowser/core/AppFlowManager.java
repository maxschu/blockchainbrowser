package com.example.max.blockchainbrowser.core;

import android.app.Activity;
import android.content.Intent;

import com.example.max.blockchainbrowser.overview.OverviewActivity;
import com.example.max.blockchainbrowser.tokens.TokenListActivity;

public class AppFlowManager {

   private Activity source;

   public AppFlowManager(Activity source) {
      this.source = source;
   }

   public void enterApp() {
      Intent intent = new Intent(source, OverviewActivity.class);
      source.startActivity(intent);
      source.finish();
   }

   public void finish() {
      source.finish();
   }

   public void onBackPressed() {
      source.onBackPressed();
   }

   public void openTokenList() {
      Intent intent = new Intent(source, TokenListActivity.class);
      source.startActivity(intent);
   }
}
