package com.example.max.blockchainbrowser.core;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public abstract class ViewHolderPresenterView<M, V, P extends Presenter<V>>
      extends PresenterView<V, P> {

   public final class ViewHolder extends RecyclerView.ViewHolder {

      ViewHolder(View itemView) {
         super(itemView);
      }

      public void bindViewHolder(M item) {
         if (getPresenter() == null) {
            bindPresenter();
         }
         updateView(item);
      }

      public M getItem() {
         return ViewHolderPresenterView.this.getItem();
      }
   }

   private ViewHolder viewHolder;

   public ViewHolderPresenterView(Context context) {
      super(context);
   }

   public ViewHolderPresenterView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   public ViewHolderPresenterView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
   }

   public abstract M getItem();

   public ViewHolder getViewHolder() {
      return viewHolder;
   }

   @Override
   protected void initialize(Context context, AttributeSet attrs, int defStyleAttr) {
      super.initialize(context, attrs, defStyleAttr);
      viewHolder = new ViewHolder(this);
   }

   protected abstract void updateView(M item);
}