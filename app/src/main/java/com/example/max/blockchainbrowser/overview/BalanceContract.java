package com.example.max.blockchainbrowser.overview;

import com.example.max.blockchainbrowser.data.models.BalanceViewModel;

interface BalanceContract {
   interface View {
      void showBalance(BalanceViewModel model);
   }
}
