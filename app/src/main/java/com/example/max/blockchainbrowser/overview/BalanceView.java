package com.example.max.blockchainbrowser.overview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.PresenterView;
import com.example.max.blockchainbrowser.data.models.BalanceViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BalanceView extends PresenterView<BalanceContract.View, BalancePresenter> implements BalanceContract.View {

    @BindView(R.id.balance_title)
    TextView balanceTitle;
    @BindView(R.id.balance)
    TextView balanceAmount;

    public BalanceView(Context context) {
        super(context);
    }

    public BalanceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BalanceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void inflatedViews() {
        super.inflatedViews();
        ButterKnife.bind(this);
    }

    @Override
    protected BalancePresenter createPresenter() {
        return new BalancePresenter();
    }

    public void showBalance(BalanceViewModel model) {
        balanceTitle.setText(model.getName());
        balanceAmount.setText(String.valueOf(model.getBalance()) + " ETH");
    }


}
