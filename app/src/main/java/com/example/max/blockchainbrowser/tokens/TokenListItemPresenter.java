package com.example.max.blockchainbrowser.tokens;

import com.example.max.blockchainbrowser.core.Presenter;

public class TokenListItemPresenter extends Presenter<TokenListItemContract.View> {

   @Override
   protected void bindView(TokenListItemContract.View view) {
      super.bindView(view);
   }

   @Override
   protected void unbindView() {
      super.unbindView();
   }
}
