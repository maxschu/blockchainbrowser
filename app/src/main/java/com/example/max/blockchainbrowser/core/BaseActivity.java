package com.example.max.blockchainbrowser.core;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.module.ActivityComponent;

public abstract class BaseActivity extends Activity {

   private static final int INVALID_LAYOUT_ID = -1;

   ActivityComponent activityComponent;

   @LayoutRes
   protected int getLayoutResourceId() {
      return INVALID_LAYOUT_ID;
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      restoreActivityComponent();
   }

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      Injector.createActivityComponent(this, getResources().getString(R.string.base_url));
      activityComponent = Injector.getActivityComponent();
      Injector.getActivityComponent()
            .inject(this);
      super.onCreate(savedInstanceState);
      setContentView(getLayoutResourceId());
   }

   @Override
   protected void onPause() {
      Injector.releaseActivityComponent();
      super.onPause();
   }

   @Override
   protected void onResume() {
      restoreActivityComponent();
      super.onResume();
   }

   private void restoreActivityComponent() {
      Injector.setActivityComponent(activityComponent);
   }
}
