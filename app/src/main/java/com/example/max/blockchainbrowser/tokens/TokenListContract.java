package com.example.max.blockchainbrowser.tokens;

import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;

import java.util.List;

interface TokenListContract {
   interface View {

      void presentTokens(List<TokenListItemViewModel>tokenList);
   }
}
