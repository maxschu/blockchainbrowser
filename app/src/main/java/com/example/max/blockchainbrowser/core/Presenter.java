package com.example.max.blockchainbrowser.core;

import android.os.Bundle;
import android.support.annotation.CallSuper;

public abstract class Presenter<V> {
   private V view;

   public void restoreInstanceState(Bundle bundle) {
      // TODO
   }

   public Bundle saveInstanceState() {
      return null;
   }

   @CallSuper
   protected void bindView(V view) {
      this.view = view;
   }

   protected V getView() {
      return view;
   }

   @CallSuper
   protected void unbindView() {
      view = null;
   }
}