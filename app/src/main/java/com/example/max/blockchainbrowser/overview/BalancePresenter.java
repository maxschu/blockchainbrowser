package com.example.max.blockchainbrowser.overview;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.Injector;
import com.example.max.blockchainbrowser.core.Presenter;
import com.example.max.blockchainbrowser.core.TokenDataBase;
import com.example.max.blockchainbrowser.data.RestApi;
import com.example.max.blockchainbrowser.data.models.BalanceViewModel;
import com.example.max.blockchainbrowser.data.models.ExchangeRateModel;
import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import jfyg.data.ERC20Token;
import jfyg.data.account.Accounts;
import retrofit2.Retrofit;

public class BalancePresenter extends Presenter<BalanceContract.View> {

    @Inject
    Context context;
    @Inject
    Retrofit retrofit;
    @Inject
    TokenDataBase dataBase;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static final int DEFAULT_DECIMAL = 18;
    private ArrayList<String> tokenSymbols;

    @Override
    protected void bindView(BalanceContract.View view) {
        super.bindView(view);
        Injector.getActivityComponent()
                .inject(this);

        tokenSymbols = new ArrayList<>();
        tokenSymbols.add("GNT");
        tokenSymbols.add("REP");
        tokenSymbols.add("OMG");
    }

    @Override
    protected void unbindView() {
        super.unbindView();
        compositeDisposable.clear();
    }

    void loadAccountBalance() {
        Accounts accounts = new Accounts();

        SingleObserver<Double> observer = new SingleObserver<Double>() {
            @Override
            public void onError(Throwable e) {
                Toast.makeText(context, "Error loading Account data", Toast.LENGTH_LONG)
                        .show();
                setModel(context.getResources()
                        .getString(R.string.account_balance), dataBase.getAccountBalance());
            }

            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Double balance) {
                double value = formatBalance(balance, DEFAULT_DECIMAL);
                dataBase.setAccountBalance(String.valueOf(value));
                setModel(context.getResources()
                        .getString(R.string.account_balance), String.valueOf(value));
            }
        };

        accounts.getBalance(context.getResources().getString(R.string.etherium_account_address))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    void loadData(final LoadingCallback loadingCallback) {
        Observable<ExchangeRateModel> call1 = retrofit.create(RestApi.class).getExchangeRate("gnt_eth");
        Observable<ExchangeRateModel> call2 = retrofit.create(RestApi.class).getExchangeRate("rep_eth");
        Observable<ExchangeRateModel> call3 = retrofit.create(RestApi.class).getExchangeRate("omg_eth");

        Observable<ExchangeRateModel> results = Observable.merge(call1, call2, call3);
        Observer<ExchangeRateModel> observer = new Observer<ExchangeRateModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(ExchangeRateModel exchangeRateModel) {
                dataBase.setExchangeRate(exchangeRateModel.getPair(), exchangeRateModel.getRate());
            }

            @Override
            public void onError(Throwable e) {
                setModel(context.getResources()
                        .getString(R.string.erc_balance), dataBase.getErcBalance());
                loadingCallback.onLoadingFinished();
            }

            @Override
            public void onComplete() {
                loadERCBalance(loadingCallback);
            }
        };

        results.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private void loadERCBalance(final LoadingCallback loadingCallback) {
        Accounts accounts = new Accounts();

        SingleObserver<List<ERC20Token>> observer = new SingleObserver<List<ERC20Token>>() {
            @Override
            public void onError(Throwable e) {
                Toast.makeText(context, "Error loading ERC data", Toast.LENGTH_LONG)
                        .show();
            }

            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(List<ERC20Token> tokens) {
                double balance = handleErcBalance(tokens);
                dataBase.setErcBalance(String.valueOf(balance));
                setModel(context.getResources()
                        .getString(R.string.erc_balance), String.valueOf(balance));
                loadingCallback.onLoadingFinished();
            }
        };

        accounts.getERC20Tokens(context.getResources().getString(R.string.etherium_account_address))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private double handleErcBalance(List<ERC20Token> tokens) {
        List<TokenListItemViewModel> tokenList = new ArrayList<>();

        double balance = 0;

        for (ERC20Token token : tokens) {
            for (String symbol : tokenSymbols) {
                if (token.getTokenSymbol() != null && token.getTokenSymbol()
                        .equals(symbol)) {
                    double tokenValue = formatBalance(Double.valueOf(token.getValue()), Integer.valueOf(token.getTokenDecimal()));
                    if (!token.getTransactionTo().equals("0x082d3e0f04664b65127876e9a05e2183451c792a")) {
                        tokenValue = tokenValue * -1;
                    }

                    balance += tokenValue * dataBase.getExchangeRate(getTokenKey(token.getTokenSymbol()));

                    if (!compareTokenList(tokenList, token, tokenValue)) {
                        TokenListItemViewModel tokenModel =
                                new TokenListItemViewModel(token.getTokenName(), String.valueOf(tokenValue),
                                        String.valueOf(tokenValue * dataBase.getExchangeRate(getTokenKey(token.getTokenSymbol()))), token.getTokenSymbol());
                        tokenList.add(tokenModel);
                    }
                }
            }
        }
        saveTokenList(tokenList);
        return balance;
    }

    private boolean compareTokenList(List<TokenListItemViewModel> tokenList, ERC20Token token, double tokenValue) {
        for (TokenListItemViewModel listToken : tokenList) {
            if (listToken.getSymbol().equals(token.getTokenSymbol())) {
                listToken.setAmount(String.valueOf(Double.valueOf(listToken.getAmount()) + tokenValue));
                return true;
            }
        }
        return false;
    }

    private String getTokenKey(String symbol) {
        return symbol.toLowerCase().concat("_eth");
    }

    private void saveTokenList(List<TokenListItemViewModel> tokenList) {
        dataBase.setTokenList(tokenList);
    }

    private void setModel(String name, String balance) {
        BalanceViewModel model = new BalanceViewModel(name, balance);
        getView().showBalance(model);
    }

    private double formatBalance(double value, int tokenDecimal) {
        return value * Math.pow(10, -tokenDecimal);
    }
}
