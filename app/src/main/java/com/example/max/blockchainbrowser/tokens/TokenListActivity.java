package com.example.max.blockchainbrowser.tokens;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.BaseActivity;

public class TokenListActivity extends BaseActivity {

   @Override
   protected int getLayoutResourceId() {
      return R.layout.token_list_activity;
   }
}