package com.example.max.blockchainbrowser.data;

import com.example.max.blockchainbrowser.data.models.ExchangeRateModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface RestApi {
    @GET("/rate/{pair}")
    Observable<ExchangeRateModel> getExchangeRate(@Path("pair") String pair);
}