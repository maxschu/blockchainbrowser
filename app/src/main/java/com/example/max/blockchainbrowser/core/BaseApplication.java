package com.example.max.blockchainbrowser.core;

import android.app.Application;

import jfyg.ApiKey;

public class BaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.init(this);
        ApiKey.Companion.getTakeOff().setApiKey("E5QFXD7ZYRH7THQM5PIXB8JD4GY38SEJZ4");
    }
}

