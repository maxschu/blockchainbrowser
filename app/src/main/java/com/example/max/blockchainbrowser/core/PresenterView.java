package com.example.max.blockchainbrowser.core;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.CallSuper;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public abstract class PresenterView<V, P extends Presenter<V>> extends FrameLayout {

   public static final String PRESENTER_STATE = "presenter state";
   private boolean inflated;
   private P presenter;

   public PresenterView(Context context) {
      super(context);
      initialize(context, null, 0);
   }

   public PresenterView(Context context, AttributeSet attrs) {
      super(context, attrs);
      initialize(context, attrs, 0);
   }

   public PresenterView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
      initialize(context, attrs, defStyleAttr);
   }

   public P getPresenter() {
      return presenter;
   }

   protected abstract P createPresenter();

   @CallSuper
   protected void inflatedViews() {

   }

   @CallSuper
   protected void initialize(Context context, AttributeSet attrs, int defStyleAttr) {
      //...
   }

   @Override
   protected void onAttachedToWindow() {
      super.onAttachedToWindow();

      if (!inflated) {
         inflatedViews();
      }
      bindPresenter();
   }

   @Override
   protected void onDetachedFromWindow() {
      unbindPresenter();
      super.onDetachedFromWindow();
   }

   @Override
   @CallSuper
   protected void onFinishInflate() {
      super.onFinishInflate();
      inflated = true;
      inflatedViews();
      bindPresenter();
   }

   @Override
   protected void onRestoreInstanceState(Parcelable state) {
      bindPresenter();
      if (state instanceof Bundle) {
         Bundle bundle = (Bundle) state;
         super.onRestoreInstanceState(bundle.getParcelable(super.getClass()
               .getSimpleName()));
         getPresenter().restoreInstanceState(bundle.getBundle(PRESENTER_STATE));
      } else {
         super.onRestoreInstanceState(state);
      }
   }

   @Override
   protected Parcelable onSaveInstanceState() {
      Bundle bundle = new Bundle();
      bundle.putParcelable(this.getClass()
            .getSimpleName(), super.onSaveInstanceState());
      bundle.putBundle(PRESENTER_STATE, getPresenter().saveInstanceState());
      return bundle;
   }

   @SuppressWarnings ("unchecked")
   protected void bindPresenter() {
      if (presenter == null) {
         presenter = createPresenter();
         presenter.bindView((V) this);
      }
   }

   private void unbindPresenter() {
      if (presenter != null) {
         presenter.unbindView();
         presenter = null;
      }
   }
}
