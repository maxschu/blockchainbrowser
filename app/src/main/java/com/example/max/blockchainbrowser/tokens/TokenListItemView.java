package com.example.max.blockchainbrowser.tokens;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.ViewHolderPresenterView;
import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenListItemView extends ViewHolderPresenterView<TokenListItemViewModel, TokenListItemContract.View,
            TokenListItemPresenter>
      implements TokenListItemContract.View {

   @BindView (R.id.token_amount)
   TextView amount;
   @BindView (R.id.token_balance)
   TextView balance;
   @BindView (R.id.token_name)
   TextView name;
   private TokenListItemViewModel model;

   public TokenListItemView(Context context) {
      super(context);
   }

   public TokenListItemView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   public TokenListItemView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
   }

   @Override
   public TokenListItemViewModel getItem() {
      return model;
   }

   @Override
   protected TokenListItemPresenter createPresenter() {
      return new TokenListItemPresenter();
   }

   @Override
   protected void inflatedViews() {
      super.inflatedViews();
      ButterKnife.bind(this);
   }

   @Override
   public void updateView(TokenListItemViewModel model) {
      this.model = model;

      name.setText(model.getName().concat(" (").concat(model.getSymbol()).concat(")"));
      amount.setText(model.getAmount());
      balance.setText(model.getBalance().concat(" ETH"));
   }
}
