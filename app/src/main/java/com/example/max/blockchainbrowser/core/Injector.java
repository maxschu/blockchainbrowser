package com.example.max.blockchainbrowser.core;

import android.app.Activity;

import com.example.max.blockchainbrowser.core.module.ActivityComponent;
import com.example.max.blockchainbrowser.core.module.ActivityModule;
import com.example.max.blockchainbrowser.core.module.AppComponent;
import com.example.max.blockchainbrowser.core.module.ApplicationModule;
import com.example.max.blockchainbrowser.core.module.DaggerAppComponent;

public final class Injector {

    private static ActivityComponent activityComponent;
    private static AppComponent appComponent;

    public static ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    static void setActivityComponent(ActivityComponent activityComponent) {
        Injector.activityComponent = activityComponent;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    static void createActivityComponent(Activity activity, String baseUrl) {
        activityComponent = appComponent.plus(new ActivityModule(activity, baseUrl, appComponent.application()));
    }

    static void init(BaseApplication application) {
        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(application))
                .build();
    }

    static void releaseActivityComponent() {
        activityComponent = null;
    }

    private Injector() {
        //should not be instantiated
    }
}
