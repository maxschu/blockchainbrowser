package com.example.max.blockchainbrowser.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TokenDataBase {

    private static final String KEY_ACOUNT_BALANCE = "accountBalance";
    private static final String KEY_ERC_BALANCE = "ercBalance";
    private static final String KEY_TOKEN_LIST = "tokenList";
    private SharedPreferences preferences;

    public TokenDataBase(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getAccountBalance() {
        return preferences.getString(KEY_ACOUNT_BALANCE, "0");
    }

    public void setAccountBalance(String balance) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_ACOUNT_BALANCE, balance);
        editor.apply();
    }

    public String getErcBalance() {
        return preferences.getString(KEY_ERC_BALANCE, "0");
    }

    public void setErcBalance(String balance) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_ERC_BALANCE, balance);
        editor.apply();
    }

    public void setExchangeRate(String key, String rate) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, rate);
        editor.apply();
    }

    public double getExchangeRate(String key) {
        return Double.valueOf(preferences.getString(key, "1"));
    }

    public List<TokenListItemViewModel> getTokenList() {
        String connectionsJSONString = preferences.getString(KEY_TOKEN_LIST, null);
        if (connectionsJSONString == null) {
            return new ArrayList<TokenListItemViewModel>();
        }
        Type type = new TypeToken<List<TokenListItemViewModel>>() {
        }.getType();
        return new Gson().fromJson(connectionsJSONString, type);
    }

    public void setTokenList(List<TokenListItemViewModel> cartList) {
        SharedPreferences.Editor editor = preferences.edit();
        String tokenListJSONString = new Gson().toJson(cartList);
        editor.remove(KEY_TOKEN_LIST);
        editor.putString(KEY_TOKEN_LIST, tokenListJSONString);
        editor.apply();
    }
}
