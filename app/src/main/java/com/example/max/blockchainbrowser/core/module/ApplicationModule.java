package com.example.max.blockchainbrowser.core.module;

import android.content.Context;

import com.example.max.blockchainbrowser.core.BaseApplication;
import com.example.max.blockchainbrowser.core.TokenDataBase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

   private final BaseApplication application;
   private final Context context;

   public ApplicationModule(BaseApplication application) {
      this.application = application;
      this.context = application.getApplicationContext();
   }

   @Provides
   @Singleton
   BaseApplication provideBaseApplication() {
      return application;
   }

   @Provides
   @Singleton
   Context provideContext() {
      return context;
   }

   @Provides
   @Singleton
   TokenDataBase provideTokenDataBase(Context context) {
      return new TokenDataBase(context);
   }
}
