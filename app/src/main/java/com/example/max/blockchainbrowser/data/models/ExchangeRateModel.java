package com.example.max.blockchainbrowser.data.models;

public class ExchangeRateModel {

    private String pair;
    private String rate;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }


}
