package com.example.max.blockchainbrowser.overview;

public interface LoadingCallback {
    void onLoadingFinished();
}
