package com.example.max.blockchainbrowser.data.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TokenListItemViewModel implements Parcelable {

   public static final Parcelable.Creator<TokenListItemViewModel> CREATOR =
         new Creator<TokenListItemViewModel>() {

            @Override
            public TokenListItemViewModel createFromParcel(Parcel parcel) {
               TokenListItemViewModel item =
                     new TokenListItemViewModel(parcel.readString(), parcel.readString(),
                           parcel.readString(), parcel.readString());
               return item;
            }

            @Override
            public TokenListItemViewModel[] newArray(int i) {
               return new TokenListItemViewModel[0];
            }
         };

   private String amount;
   private String balance;
   private String symbol;
   private String title;

   public TokenListItemViewModel(String title, String amount, String balance, String symbol) {
      this.amount = amount;
      this.title = title;
      this.balance = balance;
      this.symbol = symbol;
   }

   @Override
   public int describeContents() {
      return 0;
   }

   public String getAmount() {
      return amount;
   }

   public void setAmount(String amount) {
      this.amount = amount;
   }

   public String getBalance() {
      return balance;
   }

   public void setBalance(String balance) {
      this.balance = balance;
   }

   public String getName() {
      return title;
   }

   public String getSymbol() {
      return symbol;
   }

   public void setSymbol(String symbol) {
      this.symbol = symbol;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   @Override
   public void writeToParcel(Parcel parcel, int i) {
      parcel.writeString(title);
      parcel.writeString(amount);
      parcel.writeString(balance);
      parcel.writeString(symbol);
   }
}
