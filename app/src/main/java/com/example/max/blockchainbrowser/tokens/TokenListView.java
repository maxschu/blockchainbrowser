package com.example.max.blockchainbrowser.tokens;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;

import com.example.max.blockchainbrowser.R;
import com.example.max.blockchainbrowser.core.PresenterView;
import com.example.max.blockchainbrowser.data.models.TokenListItemViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TokenListView extends PresenterView<TokenListContract.View, TokenListPresenter>
      implements TokenListContract.View {

   @BindView (R.id.recycler_view)
   RecyclerView recyclerView;
   private TokenListAdapter adapter;

   public TokenListView(Context context) {
      super(context);
   }

   public TokenListView(Context context, AttributeSet attrs) {
      super(context, attrs);
   }

   public TokenListView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
   }

   @Override
   public void presentTokens(List<TokenListItemViewModel> tokenList) {
      if (tokenList.isEmpty()) {
         if (adapter.getItemCount() == 0) {
            Log.d("no items in list", "No items in list");
         }
      } else {
         adapter.addTokenList(tokenList);
      }
   }

   @Override
   protected TokenListPresenter createPresenter() {
      return new TokenListPresenter();
   }

   @Override
   protected void inflatedViews() {
      super.inflatedViews();
      ButterKnife.bind(this);
      initRecyclerView();
   }

   private void initRecyclerView() {
      adapter = new TokenListAdapter();
      recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
      recyclerView.setAdapter(adapter);
      recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
   }
}
