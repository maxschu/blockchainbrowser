package com.example.max.blockchainbrowser.tokens;

import com.example.max.blockchainbrowser.core.Injector;
import com.example.max.blockchainbrowser.core.Presenter;
import com.example.max.blockchainbrowser.core.TokenDataBase;

import javax.inject.Inject;

public class TokenListPresenter extends Presenter<TokenListContract.View> {

   @Inject
   TokenDataBase dataBase;

   @Override
   protected void bindView(TokenListContract.View view) {
      super.bindView(view);
      Injector.getActivityComponent()
            .inject(this);
      loadProductList();
   }

   @Override
   protected void unbindView() {
      super.unbindView();
   }

   private void loadProductList() {
      if (dataBase.getTokenList() == null) {
         return;
      }
      getView().presentTokens(dataBase.getTokenList());
   }
}
